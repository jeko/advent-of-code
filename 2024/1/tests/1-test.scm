(use-modules (gunit64))

(use-modules (ice-9 textual-ports)
	     (srfi srfi-1))

(define (puzzle-input input-file)
  
  (define (remove-last-empty-line inputs)
    (delete "" inputs))

  (define (read-all-lines filename)
    (remove-last-empty-line
     (string-split (call-with-input-file filename get-string-all) #\newline)))
  
  (fold
   (lambda (iter res)
     (list (cons (string->number (car iter)) (car res))
	   (cons (string->number (cadr iter)) (cadr res))))
   '(() ())
   (map (lambda (line)
	  (filter (lambda (elem)
		    (not (string=? elem "")))
		  (string-split line (lambda (c)
				       (equal? c #\space)))))
	(read-all-lines input-file))))


(define (total-distance l1 l2)
  (reduce + 0 (map (lambda (e1 e2) (abs (- e1 e2))) (sort l1 <) (sort l2 <))))

(define (part-one-harness)
  (test-equal "distance-between-2-lists-with-one-item-each"
    1
    (let ([l1 '(3)]
	  [l2 '(4)])
      (total-distance l1 l2)))
  (test-equal "distance-between-2-lists-with-one-item-each-bis"
    4
    (let ([l1 '(5)]
	  [l2 '(9)])
      (total-distance l1 l2)))
  (test-equal "total-distance-lists-of-two"
    0
    (let ([l1 '(3 4)]
	  [l2 '(4 3)])
      (total-distance l1 l2)))
  (test-equal "total-distance-lists-of-two-bis"
    3
    (let ([l1 '(3 8)]
	  [l2 '(9 5)])
      (total-distance l1 l2)))
  (test-equal "total-distance-example-aoc"
    11
    (let ([l1 '(3 4 2 1 3 3)]
	  [l2 '(4 3 5 3 9 3)])
      (total-distance l1 l2)))
  (test-equal "total-distance-aoc-input"
    1970720
    (let* ([input (puzzle-input "/home/jeko/Workspace/advent-of-code/2024/1/tests/input")]
	   [l1 (car input)]
	   [l2 (cadr input)])
      (total-distance l1 l2))))

(define (similarity-score l1 l2)
  (reduce
   +
   0
   (fold
    (lambda (e1 e2 previous)
      (cons
       (* e1 (length
	      (filter
	       (lambda (i)
		 (= e1 i))
	       l2)))
       previous))
    (list) l1 l2)))

(define (part-two-harness)
  (test-equal "similarity-score-between-2-lists-with-one-item"
    0
    (similarity-score '(1) '(1)))
  (test-equal "similarity-score-between-2-lists-with-two-item"
    4
    (similarity-score '(1 2) '(2 2)))
  (test-equal "similarity-score-example-aoc"
    31
    (let ([l1 '(3 4 2 1 3 3)]
	  [l2 '(4 3 5 3 9 3)])
      (similarity-score l1 l2)))
  (test-equal "similarity-score-aoc-input"
    17191599
    (let* ([input (puzzle-input "/home/jeko/Workspace/advent-of-code/2024/1/tests/input")]
	   [l1 (car input)]
	   [l2 (cadr input)])
      (similarity-score l1 l2))))

(*test-root*
 (lambda ()
   (part-one-harness)
   (part-two-harness)))
