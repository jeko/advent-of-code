(use-modules (ice-9 textual-ports)
	     (srfi srfi-1))

(define (remove-last-empty-line inputs)
  (delete "" inputs))

(define (read-all-lines filename)
  (remove-last-empty-line (string-split (call-with-input-file filename get-string-all) #\newline)))

(define (list-of-strings->list-of-numbers inputs)
  (map string->number inputs))

(define (expenses-from filename)
  (list-of-strings->list-of-numbers (read-all-lines filename)))

(let ([expenses (expenses-from "input.txt")])
  (reduce * 1 (filter (lambda (expense)
			(memq (- 2020 expense) expenses))
		      expenses)))
